/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INCLUDED_OSL_DETAIL_COMPONENT_DEFINES_H
#define INCLUDED_OSL_DETAIL_COMPONENT_DEFINES_H

/* Experimental direct constructor calls, under construction */

#define LO_URE_CURRENT_ENV 1 /*TODO*/

#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_lang_dot_RegistryServiceManager 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_lang_dot_RegistryServiceManager com_sun_star_comp_stoc_ORegistryServiceManager
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_lang_dot_ServiceManager 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_lang_dot_ServiceManager com_sun_star_comp_stoc_OServiceManager
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_loader_dot_SharedLibrary 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_loader_dot_SharedLibrary com_sun_star_comp_stoc_DLLComponentLoader
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_registry_dot_ImplementationRegistration 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_registry_dot_ImplementationRegistration com_sun_star_comp_stoc_ImplementationRegistration
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_registry_dot_NestedRegistry 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_registry_dot_NestedRegistry com_sun_star_comp_stoc_NestedRegistry
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_registry_dot_SimpleRegistry 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_registry_dot_SimpleRegistry com_sun_star_comp_stoc_SimpleRegistry
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_security_dot_AccessController 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_security_dot_AccessController com_sun_star_security_comp_stoc_AccessController
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_security_dot_Policy 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_security_dot_Policy com_sun_star_security_comp_stoc_FilePolicy
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_xml_dot_sax_dot_FastParser 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_xml_dot_sax_dot_FastParser com_sun_star_comp_extensions_xml_sax_FastParser
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_xml_dot_sax_dot_Parser 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_xml_dot_sax_dot_Parser com_sun_star_comp_extensions_xml_sax_ParserExpat
#define LO_URE_CTOR_ENV_com_dot_sun_dot_star_dot_xml_dot_sax_dot_Writer 1 /*TODO*/
#define LO_URE_CTOR_FUN_com_dot_sun_dot_star_dot_xml_dot_sax_dot_Writer com_sun_star_extensions_xml_sax_Writer

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
